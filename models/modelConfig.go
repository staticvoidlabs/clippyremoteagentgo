package models

// Config struct defines the data model to hold the current configuration given in "config.json".
type Config struct {
	Version                    string `json:"version"`
	DebugLevel                 int    `json:"debugLevel"`
	RemoteClippyBaseURL        string `json:"remoteClippyBaseURL"`
	RemoteClippyRouteAdd       string `json:"remoteClippyRouteAdd"`
	RemoteClippyURLRouteStatus string `json:"remoteClippyURLRouteStatus"`
	SendIdInsteadOfUrl         bool   `json:"sendIdInsteadOfUrl"`
	RefreshQueueStatus         bool   `json:"refreshQueueStatus"`
}

// DownloadJob struct defines the data model to hold a download job object.
type DownloadJob struct {
	VideoID         string
	VideoTitle      string
	URL             string
	FilePath        string
	FileExtSet      bool
	FileSize        string
	Priority        int
	State           string
	StateInfo       string
	StateHasChanged bool
}

// Queue struct defines the data model to hold a download job object.
type Queue struct {
	Jobs []DownloadJob
}
