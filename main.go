package main

import (
	"fmt"
	"strings"
	"time"

	"github.com/atotto/clipboard"
	"gitlab.com/staticvoidlabs/crag/manager"
	"gitlab.com/staticvoidlabs/crag/models"
)

var mShouldExit = false
var mCurrentConfig models.Config
var mLastAction string = ""

func main() {

	// Init config.
	mCurrentConfig = manager.GetCurrentConfig()

	fmt.Println("Clippy Remote Agent (Version " + manager.GetVersionInfo() + ")")
	fmt.Println("")

	// Enter the main loop.
	for !mShouldExit {

		// Update queue status.
		manager.UpdateQueueStatus()

		// Process clipboard content.
		tmpClipboardContent, err := clipboard.ReadAll()

		if err != nil && err.Error() != "Der Vorgang wurde erfolgreich beendet." {
			fmt.Println(err)
		}

		tmpClipboardContent = strings.TrimSpace(tmpClipboardContent)

		if mLastAction != tmpClipboardContent {

			mLastAction = tmpClipboardContent

			manager.ProcessClipboardLink(tmpClipboardContent)
		}

		time.Sleep(1 * time.Second)
	}

}
