module gitlab.com/staticvoidlabs/crag

go 1.22.3

require (
	github.com/atotto/clipboard v0.1.4
	github.com/inancgumus/screen v0.0.0-20190314163918-06e984b86ed3
)

require (
	golang.org/x/crypto v0.24.0 // indirect
	golang.org/x/sys v0.21.0 // indirect
	golang.org/x/term v0.21.0 // indirect
)
