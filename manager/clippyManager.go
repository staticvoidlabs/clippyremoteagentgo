package manager

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/inancgumus/screen"
	"gitlab.com/staticvoidlabs/crag/models"
)

const colorReset = "\033[0m"
const colorRed = "\033[31m"
const colorGreen = "\033[32m"
const colorYellow = "\033[33m"
const colorBlue = "\033[34m"
const colorPurple = "\033[35m"
const colorCyan = "\033[36m"
const colorWhite = "\033[97m"
const colorGray = "\033[37m"

var mCurrentQueue []models.DownloadJob

func addVideoToRemoteQueue(videoInfo string) {

	fmt.Println("")

	if mCurrentConfig.DebugLevel == 1 {
		fmt.Println("   Valid URL detected: " + mCurrentConfig.RemoteClippyBaseURL + mCurrentConfig.RemoteClippyRouteAdd + videoInfo)
	}

	res, err := http.Get(mCurrentConfig.RemoteClippyBaseURL + mCurrentConfig.RemoteClippyRouteAdd + videoInfo)

	if err != nil && mCurrentConfig.DebugLevel == 1 {
		fmt.Printf("   Error making http request: %s\n", err)
	} else {

		if mCurrentConfig.DebugLevel == 1 {
			fmt.Printf("   Added video to queue with RC %d\n", res.StatusCode)
		}

	}

}

func UpdateQueueStatus() {

	// Get current queue from remote machine.
	res, err := http.Get(mCurrentConfig.RemoteClippyBaseURL + mCurrentConfig.RemoteClippyURLRouteStatus)

	if err != nil && mCurrentConfig.DebugLevel == 1 {
		fmt.Printf("   Error making http request: %s\n", err)
	} else {

		if mCurrentConfig.DebugLevel == 1 {
			fmt.Printf("   Updated queue with RC %d\n", res.StatusCode)
		}

		body, error := ioutil.ReadAll(res.Body)
		if error != nil {
			fmt.Println(error)
		}

		// Unmarshall JSON.
		json.Unmarshal([]byte(body), &mCurrentQueue)

		res.Body.Close()

		if mCurrentConfig.DebugLevel == 2 {
			fmt.Println(string(body))
		}

		showQueueStateInConsole()
	}

}

func showQueueStateInConsole() {

	clearConsoleOutput()

	fmt.Println("Clippy Remote Agent (Version " + mCurrentConfig.Version + ")")
	fmt.Println("")

	for i, tmpJob := range mCurrentQueue {

		if mCurrentConfig.DebugLevel > 0 {
			fmt.Println(strconv.Itoa(i) + " > " + tmpJob.VideoID + " > " + tmpJob.State + " > " + tmpJob.VideoTitle)
		} else {

			if tmpJob.State != "" {

				switch state := tmpJob.State; state {
				case "DOWNLOADING":
					fmt.Println(string(colorYellow), "   "+tmpJob.State, string(colorReset)+"      "+tmpJob.FileSize+"   "+tmpJob.VideoTitle)
				case "FAILED":
					fmt.Println(string(colorRed), "        "+tmpJob.State, string(colorReset)+"      "+tmpJob.FileSize+"   "+tmpJob.VideoTitle)
				case "FINISHED":
					fmt.Println(string(colorGreen), "      "+tmpJob.State, string(colorReset)+"      "+tmpJob.FileSize+"   "+tmpJob.VideoTitle)
				default:
					fmt.Println("Unknown state for job " + strconv.Itoa(i))
				}

			}

		}

	}

}

func clearConsoleOutput() {
	screen.Clear()
	screen.MoveTopLeft()
}
