package manager

import (
	"fmt"
	"strings"
)

func ProcessClipboardLink(videoInfo string) {

	// Get first 23 chars of current clipboard content.
	tmpRunes := []rune(videoInfo)
	tmpSubstringStart := string(tmpRunes[0:23])

	if tmpSubstringStart == "https://www.youtube.com" {

		// Extract video ID out of URL.
		tmpStrings := strings.Split(videoInfo, "?v=")

		// Send URL or ID to remote queue.
		if mCurrentConfig.SendIdInsteadOfUrl {

			if len(tmpStrings) == 2 && tmpStrings[1] != "" {
				go addVideoToRemoteQueue(tmpStrings[1])
			}
		} else {
			go addVideoToRemoteQueue(videoInfo)
		}

	} else {
		fmt.Println("   Skipping: " + videoInfo)
	}

}
