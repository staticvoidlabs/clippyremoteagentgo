package manager

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/staticvoidlabs/crag/models"
)

var mCurrentConfig models.Config

// Public functions.
func GetCurrentConfig() models.Config {

	return processConfigFile()
}

// Private functions.
func processConfigFile() models.Config {

	var currentConfig models.Config

	configFile, err := os.Open("./config.json")

	if err != nil {
		fmt.Println(err.Error())
	}

	defer configFile.Close()

	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&currentConfig)
	mCurrentConfig = currentConfig

	return currentConfig
}

func GetVersionInfo() string {

	tmpVersionInfo := "n/a"

	if mCurrentConfig.Version != "" {
		tmpVersionInfo = mCurrentConfig.Version
	}

	return tmpVersionInfo
}
